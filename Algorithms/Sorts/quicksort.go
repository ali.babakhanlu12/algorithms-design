package main

import "fmt"

func quickSort(arr []int, low, high int) {
	if low < high {
		// Partition the array
		pivotIndex := partition(arr, low, high)

		// Recursively sort the two subarrays
		quickSort(arr, low, pivotIndex-1)
		quickSort(arr, pivotIndex+1, high)
	}
}

func partition(arr []int, low, high int) int {
	// Choose the rightmost element as the pivot
	pivot := arr[high]

	// Index of the smaller element
	i := low - 1

	for j := low; j < high; j++ {
		// If current element is smaller than or equal to the pivot
		if arr[j] <= pivot {
			i++
			// Swap arr[i] and arr[j]
			arr[i], arr[j] = arr[j], arr[i]
		}
	}

	// Swap arr[i+1] and arr[high] (pivot)
	arr[i+1], arr[high] = arr[high], arr[i+1]

	return i + 1
}

func main() {
	arr := []int{9, 5, 1, 3, 8, 4, 2, 7, 6}
	fmt.Println("Original array:", arr)

	quickSort(arr, 0, len(arr)-1)
	fmt.Println("Sorted array:", arr)
}
