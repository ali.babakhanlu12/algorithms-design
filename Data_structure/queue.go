package main

import "fmt"

type Queue struct {
	data []int
}

func (q *Queue) Enqueue(value int) {
	q.data = append(q.data, value)
}

func (q *Queue) Dequeue() (int, bool) {
	if q.IsEmpty() {
		return 0, false
	}

	value := q.data[0]
	q.data = q.data[1:]

	return value, true
}

func (q *Queue) IsEmpty() bool {
	return len(q.data) == 0
}

func main() {
	queue := Queue{}

	queue.Enqueue(1)
	queue.Enqueue(2)
	queue.Enqueue(3)

	fmt.Println("Is queue empty?", queue.IsEmpty())

	for !queue.IsEmpty() {
		value, _ := queue.Dequeue()
		fmt.Println("Dequeued:", value)
	}

	fmt.Println("Is queue empty?", queue.IsEmpty())
}
