package main

import "fmt"

type Stack struct {
	data []int
}

func (s *Stack) Push(value int) {
	s.data = append(s.data, value)
}

func (s *Stack) Pop() (int, bool) {
	if s.IsEmpty() {
		return 0, false
	}

	index := len(s.data) - 1
	value := s.data[index]
	s.data = s.data[:index]

	return value, true
}

func (s *Stack) IsEmpty() bool {
	return len(s.data) == 0
}

func main() {
	stack := Stack{}

	stack.Push(1)
	stack.Push(2)
	stack.Push(3)

	fmt.Println("Is stack empty?", stack.IsEmpty())

	for !stack.IsEmpty() {
		value, _ := stack.Pop()
		fmt.Println("Popped:", value)
	}

	fmt.Println("Is stack empty?", stack.IsEmpty())
}
